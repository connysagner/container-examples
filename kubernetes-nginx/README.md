Example of a Kubernetes configuration for Nginx with load balancing

## Usage ##

Start

    kubectl apply -f nginx.yaml

Show information about pods and services

    kubectl get pods

    kubectl get services

Stop 

    kubectl delete -f nginx.yaml